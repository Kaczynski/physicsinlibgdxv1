package lukasz.kaczynski.managers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import lukasz.kaczynski.utils.Constants;

/**
 *
 */
public class CameraManager {

    public OrthographicCamera gamecam;
    public Viewport gamePort;
    public float zoom = 1f;
    public CameraManager(float zoom) {
        this.zoom = zoom;
        init();
    }
    public CameraManager(float zoom, Vector2 point) {
        this.zoom = zoom;
        init();
        setGamecamPosition(point);
    }
    public CameraManager(Vector2 point) {
        init();
        setGamecamPosition(point);
    }
    public CameraManager() {
        init();
    }

    private void init() {
        gamecam = new OrthographicCamera();
        gamecam.zoom = zoom;
        gamePort = new FitViewport(Constants.SCREEN_WIDTH  , Constants.SCREEN_HEIGHT  , gamecam);
    }

    public void setGamecamPosition(Vector2 point){
        gamecam.position.y = point.y;
                gamecam.position.x =point.x;
    }
    public void findPosition(float x, float y, float dt , float camSpeed) {
        if (y < gamePort.getWorldHeight() / 2 * gamecam.zoom) {
            gamecam.position.y += (gamePort.getWorldHeight() / 2 * gamecam.zoom - gamecam.position.y) * camSpeed * dt;
        } else {
            gamecam.position.y += (y - gamecam.position.y) * camSpeed * dt;
        }
        if (x < gamePort.getWorldWidth() / 2 * gamecam.zoom) {
            gamecam.position.x += (gamePort.getWorldWidth() / 2 * gamecam.zoom - gamecam.position.x) * camSpeed * dt;
        } else {
            gamecam.position.x += (x - gamecam.position.x) * camSpeed * dt;
        }
    }
}
