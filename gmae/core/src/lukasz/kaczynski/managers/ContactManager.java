package lukasz.kaczynski.managers;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

/**
 * Class responsible for processing the events produced by the
 * collisions between the different bodies shapes that exist in the world
 * A body can have several collision shapes (head body an legs for example) and can trigger different functions.
 */
public class ContactManager implements ContactListener {

    /**
     * Function activated when two bodies shapes collide
     * @param contact Contact
     */
    @Override
    public void beginContact(Contact contact) {

    }

    /**
     * Function activated when two bodies shapes stop colliding collide
     * @param contact
     */
    @Override
    public void endContact(Contact contact) {

    }

    /**
     * Function activated when two bodies shapes are going to collide
     * @param contact
     * @param oldManifold
     */
    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    /**
     * Function activated when two bodies shapes stop colliding and the enContact is executed
     * @param contact
     * @param impulse
     */
    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
