package lukasz.kaczynski.managers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import lukasz.kaczynski.screens.GameScreen;
import lukasz.kaczynski.utils.Constants;


/**
 * Class responsible for loading all elements of the current level
 */
public class LevelManager {

    public OrthogonalTiledMapRenderer mapRender;
    private TiledMap map;
    public World world;
    private GameScreen screen;
    private Box2DDebugRenderer b2dr;

    public LevelManager(GameScreen screen) {
        this.screen = screen;
        b2dr = new Box2DDebugRenderer(); //shows the collision bodies
        world = new World(new Vector2(0, -10), true);// Gravity x, y, Allows the bodies sleeps
    }

    /**
     * function responsible for loading the map
     * @param levelName
     */
    public void loadCurrentMap(String levelName) {

        map = new TmxMapLoader().load(levelName);
        mapRender = new OrthogonalTiledMapRenderer(map, 1);

        screen.cameraManager.gamecam.position.set(screen.cameraManager.gamePort.getWorldWidth() / 2, screen.cameraManager.gamePort.getWorldHeight() / 2, 0);
        loadMap();
        world.setContactListener(new ContactManager());
    }

    /**
     * Load map objects
     */
    private void loadMap() {

        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        //GROUND
        for (MapObject object : map.getLayers().get("ground").getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();
            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) , (rect.getY() + rect.getHeight() / 2) );

            body = world.createBody(bdef);
            shape.setAsBox(rect.getWidth() / 2 , rect.getHeight() / 2);
            fdef.shape = shape;
            body.createFixture(fdef);
        }

    }

    /**
     * Update the positions of the level elements such as the player, enemies, blocks and more
     * @param dt
     */
    public void update(float dt) {
        world.step(1 / 60f, 6, 2);
        mapRender.setView(screen.cameraManager.gamecam);
    }


    /**
     * draws the map objects
      * @param dt float
     * @param batch SpriteBatch
     */
    public void draw(float dt, SpriteBatch batch) {

        b2dr.render(world,screen.cameraManager.gamecam.combined);
    }

}
