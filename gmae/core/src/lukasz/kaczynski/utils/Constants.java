package lukasz.kaczynski.utils;

public class Constants {
    public static final int SCREEN_WIDTH=1920;
    public static final int SCREEN_HEIGHT=1080;
    public static final String LEVEL_FOLDER = "levels";

    public static final float PIXELS_PER_MITER =400;

    public static final short BIT_GROUND =1;
    public static final short BIT_PLAYER =2;
    public static final short BIT_BRICK =4;
    public static final short BIT_COIN =8;
    public static final short BIT_DESTROYED =16;
    public static final short BIT_OBJECT = 32;
    public static final short BIT_ENEMY = 64;
    public static final short BIT_ENEMY_HEAD = 128;
    public static final short BIT_AMMO = 256;
}

