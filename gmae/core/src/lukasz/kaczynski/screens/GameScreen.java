package lukasz.kaczynski.screens;

import com.badlogic.gdx.Screen;
import lukasz.kaczynski.PhysicsInLibgdx;
import lukasz.kaczynski.managers.CameraManager;
import lukasz.kaczynski.managers.LevelManager;
import lukasz.kaczynski.utils.Constants;

public class GameScreen implements Screen {

    private PhysicsInLibgdx game;
    public CameraManager cameraManager;
    public LevelManager levelManager;


    public GameScreen(PhysicsInLibgdx game) {
        this.game = game;
        this.cameraManager = new CameraManager();
        this.levelManager = new LevelManager(this);
        levelManager.loadCurrentMap(Constants.LEVEL_FOLDER + "/ejemplo.tmx");
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
