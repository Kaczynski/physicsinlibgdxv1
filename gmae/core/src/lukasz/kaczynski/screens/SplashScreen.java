package lukasz.kaczynski.screens;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import lukasz.kaczynski.PhysicsInLibgdx;
import lukasz.kaczynski.managers.CameraManager;
import lukasz.kaczynski.managers.ResourceManager;

public class SplashScreen implements Screen {

    private float dtTotal = 0;
    private boolean splashDone = false;

    private PhysicsInLibgdx game;
    private Texture splashTexture;
    private Image splashImage;
    private CameraManager cam;
    private Stage stage;

    public SplashScreen(PhysicsInLibgdx game) {
        this.game = game;

        cam = new CameraManager(0.9f);

        splashTexture = new Texture(Gdx.files.internal("ui/snapLogo.png"));
        splashImage = new Image(splashTexture);
        splashImage.setPosition((cam.gamePort.getWorldWidth() - splashImage.getWidth()) / 2, (cam.gamePort.getWorldHeight() - splashImage.getHeight()) / 2);
        splashImage.setDrawable(new TextureRegionDrawable(new TextureRegion(splashTexture)));
        splashImage.setSize(splashTexture.getWidth(), splashTexture.getHeight());
        ResourceManager.loadAllResources();

        stage = new Stage(cam.gamePort);
        stage.addActor(splashImage);
        //stage.setDebugAll(true);
    }

    public void update(float dt) {
        stage.act(dt);
        if (ResourceManager.update()) {
            // Si la animación ha terminado se muestra ya el menú principal
            if (splashDone) {
                dtTotal += dt;
                if (dtTotal > 2) {
                    game.setScreen(new GameScreen(game));
                }

            }
        }
    }

    @Override
    public void show() {
        splashImage.addAction(Actions.sequence(Actions.alpha(0), Actions.fadeIn(1f),
                Actions.delay(0.5f), Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        splashDone = true;
                    }
                })
        ));
    }

    @Override
    public void render(float delta) {
        update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        game.batch.setProjectionMatrix(cam.gamecam.combined);
        game.batch.begin();

        stage.draw();
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {
        cam.gamePort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        splashTexture.dispose();
    }
}
